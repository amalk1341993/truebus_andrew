import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {

  constructor(private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {
  }

   goToPage(path,data=null){
    console.log(data)
    this.router.navigateByUrl(path,{queryParams:data});
    document.body.scrollTop = document.documentElement.scrollTop = 0;
  }

}
