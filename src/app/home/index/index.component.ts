import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';

import * as moment from 'moment';


@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit {


  constructor(private router: Router,private route: ActivatedRoute,private _localeService: BsLocaleService) { 
    this._localeService.use('engb');
  }

  ngOnInit() {
  }


 goToPage(path,data=null){
    console.log(data)
    this.router.navigateByUrl(path,{queryParams:data});
    document.body.scrollTop = document.documentElement.scrollTop = 0;
  }
}
