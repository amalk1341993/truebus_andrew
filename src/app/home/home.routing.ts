import { ModuleWithProviders } from '@angular/core'
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import {SearchresultComponent} from './searchresult/searchresult.component';
import {BookingComponent} from './booking/booking.component';
import { AccountComponent } from './account/account.component';

const ModuleRoutes: Routes = [

	{
		path: 'index',
		component: IndexComponent
	},
	{
		path: "searchresult",
		component: SearchresultComponent
	},
	{
		path: "booking",
		component: BookingComponent
	},
	{
		path: "account",
		component: AccountComponent
	}		

];

export const moduleRouting: ModuleWithProviders = RouterModule.forChild(ModuleRoutes)

