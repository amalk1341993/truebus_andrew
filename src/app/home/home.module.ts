import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {moduleRouting} from './home.routing';

/* BOOTSTRAP-COMPONENTS */

import { BsDatepickerModule } from 'ngx-bootstrap';

import { defineLocale } from 'ngx-bootstrap/chronos';

import { enGbLocale } from 'ngx-bootstrap/locale';

defineLocale('engb', enGbLocale);





/* CUSTOM-COMPONENTS */

import { IndexComponent } from './index/index.component';
import { SearchresultComponent } from './searchresult/searchresult.component';
import { BookingComponent } from './booking/booking.component';
import { AccountComponent } from './account/account.component';



@NgModule({
  imports: [
    CommonModule,
    BsDatepickerModule,
    moduleRouting,
    BsDatepickerModule.forRoot()
  ],
  declarations: [
    IndexComponent,
    SearchresultComponent,
    BookingComponent,
    AccountComponent
  ]
})
export class HomeModule { }
