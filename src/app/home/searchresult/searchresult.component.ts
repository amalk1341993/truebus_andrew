import { Component, OnInit, Input, Output, EventEmitter, } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-searchresult',
  templateUrl: './searchresult.component.html',
  styleUrls: ['./searchresult.component.scss']
})
export class SearchresultComponent implements OnInit {

  constructor(private router: Router,private route: ActivatedRoute) { }


  @Input() rating: number;
  @Input() itemId: number;
  @Output() ratingClick: EventEmitter<any> = new EventEmitter<any>();

  inputName: string;
  ngOnInit() {
    this.inputName = this.itemId + '_rating';
  }
  onClick(rating: number): void {
    this.rating = rating;
    this.ratingClick.emit({
      itemId: this.itemId,
      rating: rating
    });
  }

  goToPage(path,data=null){
    console.log(data)
    this.router.navigateByUrl(path,{queryParams:data});
    document.body.scrollTop = document.documentElement.scrollTop = 0;
  }

}
