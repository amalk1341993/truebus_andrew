
import { Component, OnInit, HostListener, TemplateRef, ElementRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { FormControl, FormGroup, FormBuilder, Validators, ValidationErrors } from '@angular/forms';
import { ValidationService } from './../provider/validation.service';
import { WebService } from './../provider/web.service';
import { SubjectService } from './../provider/subject.service';
import { Login } from '../provider/declare.model';


declare var $;
declare const window: any;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})


export class NavbarComponent implements OnInit {

modalRef: BsModalRef;
signForm: FormGroup;
forgotForm: FormGroup;
loginForm: FormGroup;
@ViewChild('login') private login: TemplateRef<HTMLElement>;

mobnumPattern = '^((\\+?)|0)?[0-9]{0,20}$';
emailPattern = '^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';

email_id = new FormControl('', [Validators.required, Validators.maxLength(50), Validators.pattern(this.emailPattern)]);
password = new FormControl('', [Validators.required, Validators.minLength(6)]);
name = new FormControl('', [Validators.required]);
mob = new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(15), Validators.pattern(this.mobnumPattern)]);
term = new FormControl('', [Validators.required, Validators.pattern('true')]);
loading: boolean;
loginSubmit: boolean;
SignSubmit: boolean;
funcName: string;
loginDetails: any;
loginResponse: any;
signResponse: any;
forgotResponse: any;
forgotSubmit: any;

constructor(
  private router: Router,
  private route: ActivatedRoute,
  private bsmodalservice: BsModalService,
  private formBuilder: FormBuilder,
  public vs: ValidationService,
  public service: WebService,
  public subjectService: SubjectService
  ) {
    this.loading = false;
    this.loginSubmit = false;
    this.SignSubmit = false;
    this.forgotSubmit = false;
  }

  ngOnInit() {
    this.subjectService.getLoginData().subscribe(loginData => {
      this.loginDetails = JSON.parse(this.service.getLocalStorageItem('userData'));
    });
  }



  @HostListener('window:scroll', [])
  onWindowScroll() {

    const number = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;
    if (number > 100) {
      console.log('You are 100px from the top to bottom');
    } else if (number > 500) {
        console.log('You are 500px from the top to bottom');
    }

    $(window).scroll(function() {
      const sticky = $('.navbar'),
        scroll = $(window).scrollTop();
        if (scroll > 100) { sticky.addClass('scrolled_navbar'); } else { sticky.removeClass('scrolled_navbar'); }
    });
  }

   goToPage(path, data = null) {
    console.log(data);
    this.router.navigateByUrl(path, {queryParams: data});
    document.body.scrollTop = document.documentElement.scrollTop = 0;
  }

  form_initalize(type: string) {
    switch (type) {
      case 'login':
        this.loginForm = this.formBuilder.group({
          username: this.email_id,
          password: this.password
        });
      break;
      case 'signup':
        this.signForm = this.formBuilder.group({
          username: this.email_id,
          password: this.password,
          name: this.name,
          mob: this.mob,
          terms: this.term,
        });
      break;
      case 'forgot':
        this.forgotForm = this.formBuilder.group({
          username: this.email_id
        });
      break;
    }

    if (this.forgotForm) {
      this.forgotForm.reset();
    } else if (this.signForm) {
      this.signForm.reset();
    } else {
      this.loginForm.reset();
    }
  }

  open(template: TemplateRef<any>, type: string) {
    this.form_initalize(type);
    this.modalRef = this.bsmodalservice.show(template, {backdrop: 'static'});
  }

  close() {
    this.modalRef.hide();
  }

  loginProcess() {
    const This = this;
    console.log(this.loginForm.value);
    this.loginSubmit = true;
    if (this.loginForm.valid) {
      const userObj = this.loginForm.value;
      this.funcName = 'user_login';
      this.loading = true;
      this.service.post_data(this.funcName, userObj).subscribe(response => {
        this.loading = false;
        if ( response.status === 'success') {
          this.service.setLocalStorageItem('userData', JSON.stringify(response.data));
          this.subjectService.sendLoginData('logged_in');
          this.loginResponse = response;
          setTimeout( function() {
            This.loginResponse.status = '';
            This.loginResponse.message = '';
            This.loginForm.reset();
            This.close();
          }, 2000);
        } else {
          this.loginResponse = response;
        }
      }, (error) => {

      });
    } else {
      this.loading = false;
      this.getErrors(this.loginForm);
    }
    console.log('tested');
  }

  signProcess() {
    const This = this;
    console.log(this.signForm.value);
    this.SignSubmit = true;
    if (this.signForm.valid) {
      const userObj = this.signForm.value;
      this.funcName = 'user_signup';
      this.loading = true;
      this.service.post_data(this.funcName, userObj).subscribe(response => {
        this.loading = false;
        if ( response.status === 'success') {
          this.service.setLocalStorageItem('userData', JSON.stringify(response.data));
          this.subjectService.sendLoginData('logged_in');
          this.signResponse = response;
          setTimeout( function() {
            This.signResponse.status = '';
            This.signResponse.message = '';
            This.signForm.reset();
            This.close();
          }, 2000);
        } else {
          this.signResponse = response;
        }
      }, (error) => {

      });
    } else {
      this.loading = false;
      this.getErrors(this.signForm);
    }
    console.log('tested');
  }

  forgotProcess() {
    const This = this;
    console.log(this.forgotForm.value);
    this.forgotSubmit = true;
    if (this.forgotForm.valid) {
      const userObj = this.forgotForm.value;
      this.funcName = 'user_forgot';
      this.loading = true;
      this.service.post_data(this.funcName, userObj).subscribe(response => {
        this.loading = false;
        if ( response.status === 'success') {
          this.loginResponse = response;
          This.forgotForm.reset();
          this.open(this.login, 'login');
          setTimeout( function() {
            This.loginResponse.status = '';
            This.loginResponse.message = '';
          }, 2000);
        } else {
          this.forgotResponse = response;
        }
      }, (error) => {

      });
    } else {
      this.loading = false;
      this.getErrors(this.signForm);
    }
  }

  getErrors(FormAttr) {
    Object.keys(FormAttr.controls).forEach(key => {
      console.log(key);
      const controlErrors: ValidationErrors = FormAttr.get(key).errors;
      if (controlErrors != null) {
          Object.keys(controlErrors).forEach(keyError => {
            console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
          });
        }
    });
  }

  logout() {
    const userData = this.service.getLocalStorageItem('userData');
    this.service.removeLocalStorageItem(userData);
    this.subjectService.sendLoginData('logged_out');
    this.router.navigate(['../index']);
}


}
