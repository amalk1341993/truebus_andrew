import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeModule} from './home/home.module';
import { AppComponent } from './app.component';
import {AppRoutingModule} from './app.routing';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';

import { AuthService } from './provider/auth.service';
import { AuthGuard } from './provider/auth.guard';
import { HttpErrorHandler, HandleError } from './provider/http-error-handler.service';
import { WebService } from './provider/web.service';
import { MessageService } from './provider/message.service';


import { ModalModule } from 'ngx-bootstrap';


import { defineLocale } from 'ngx-bootstrap/chronos';
import { deLocale } from 'ngx-bootstrap/locale';
defineLocale('de', deLocale);

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HomeModule,
    ReactiveFormsModule,
    FormsModule,
    ModalModule.forRoot(),
  ],
  providers: [AuthGuard, AuthService, HttpErrorHandler, WebService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
