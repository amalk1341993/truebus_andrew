import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { apiConfig } from '../../environments/server.config';
import { HttpErrorHandler, HandleError } from './http-error-handler.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Auth': 'my_key'
  })
};

@Injectable({
  providedIn: 'root'
})

export class WebService {
  private handleError: HandleError;
  constructor(private http: HttpClient, httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError('WebService');
  }

  post_data(url: string, data) {
    return this.http.post(`${apiConfig + url}`, data, httpOptions).pipe(map((response: any) => response));
  }
  get_data(url: string) {
    return this.http.get(`${apiConfig + url}`, httpOptions).pipe(map((response: any) => response));
  }
  setLocalStorageItem(id: string, data: string) {
    localStorage.setItem(id, data);
  }
  getLocalStorageItem(data: string): string {
      return localStorage.getItem(data);
  }
  removeLocalStorageItem(data) {
    localStorage.removeItem(data);
    localStorage.clear();
  }
}
