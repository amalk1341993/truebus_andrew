import { Injectable } from '@angular/core';
import { observable, Observable } from 'rxjs';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {
  private loginDetails = new BehaviorSubject<any>(null);

  sendLoginData(loginData: any) {
    this.loginDetails.next(loginData);
  }

  getLoginData(): Observable<any> {
    return this.loginDetails.asObservable();
  }

}
