export class Login {
    login_post: Loggeddata;
    sign_post: SignupData;
    forgot_post: string;
}

export class Loggeddata {
    username: string;
    password: string;
}

export class SignupData {
    name: string;
    username: string;
    password: string;
    mob: string;
}
